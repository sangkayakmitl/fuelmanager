<?php

function getavrconsumtion()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select (max(fuel_record_mile)-min(fuel_record_mile))/sum(fuel_record_liter) as avrcons FROM fuelconsumtiondb.fuel_record_tbl;";

    $avrconsumtion=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $avrconsumtion=$row['avrcons'];
    mysqli_free_result($result);
    mysqli_close($link);
    $avrconsumtion = number_format($avrconsumtion, 2, '.', ',');
    return $avrconsumtion;
}

function getmaintenancecost()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht) as maintenancecost FROM fuelconsumtiondb.fuel_cost_tbl where fuel_cost_type = 'ซ่อมบำรุง';";

    $maintenancecost=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $maintenancecost=$row['maintenancecost'];
    mysqli_free_result($result);
    mysqli_close($link);
    $maintenancecost = number_format($maintenancecost, 0, '.', ',');
    return $maintenancecost;
}

function getfuelcost()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht) as fuelcost FROM fuelconsumtiondb.fuel_cost_tbl where fuel_cost_type = 'เติมน้ำมัน';";

    $fuelcost=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $fuelcost=$row['fuelcost'];
    mysqli_free_result($result);
    mysqli_close($link);
    $fuelcost = number_format($fuelcost, 0, '.', ',');
    return $fuelcost;
}

function getgascost()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht) as gascost FROM fuelconsumtiondb.fuel_cost_tbl where fuel_cost_type = 'เติมแก๊ส';";

    $gascost=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $gascost=$row['gascost'];
    mysqli_free_result($result);
    mysqli_close($link);
    $gascost = number_format($gascost, 0, '.', ',');
    return $gascost;
}

function getoveralloutgoingrate()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl;";

    $outgoingrate=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $outgoingrate=$row['outgoing'];
    mysqli_free_result($result);
    mysqli_close($link);
    $outgoingrate = number_format($outgoingrate, 2, '.', ',');
    return $outgoingrate;
}

function getgasoutgoingrate()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    // $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    // where fuel_cost_type not like 'ซ่อมบำรุง';";

    $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    where fuel_cost_type like 'เติมแก๊ส' or fuel_cost_type like 'เริ่มต้น'";
    

    $outgoingrate=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $outgoingrate=$row['outgoing'];
    mysqli_free_result($result);
    mysqli_close($link);
    $outgoingrate = number_format($outgoingrate, 2, '.', ',');
    return $outgoingrate;
}

function getfueloutgoingrate()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    where fuel_cost_type not like 'ซ่อมบำรุง';";

    // $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    // where fuel_cost_type like 'เติมแก๊ส' or fuel_cost_type like 'เริ่มต้น';";
    

    $outgoingrate=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $outgoingrate=$row['outgoing'];
    mysqli_free_result($result);
    mysqli_close($link);
    $outgoingrate = number_format($outgoingrate, 2, '.', ',');
    return $outgoingrate;
}

function getfueloutgoingrate3month()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    where (fuel_cost_type ='เติมแก๊ส' or fuel_cost_type ='เติมน้ำมัน')and DATE(fuel_cost_date) >= (DATE(NOW()) - INTERVAL 90 DAY);";

    // $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    // where fuel_cost_type like 'เติมแก๊ส' or fuel_cost_type like 'เริ่มต้น';";
    

    $outgoingrate=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $outgoingrate=$row['outgoing'];
    mysqli_free_result($result);
    mysqli_close($link);
    $outgoingrate = number_format($outgoingrate, 2, '.', ',');
    return $outgoingrate;
}

function getgasoutgoingrate3month()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    // $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    // where fuel_cost_type not like 'ซ่อมบำรุง';";

    $sql="select sum(fuel_cost_baht)/(max(fuel_cost_distance)-min(fuel_cost_distance))  as outgoing FROM fuelconsumtiondb.fuel_cost_tbl
    where (fuel_cost_type ='เติมแก๊ส') and DATE(fuel_cost_date) >= (DATE(NOW()) - INTERVAL 90 DAY);";
    

    $outgoingrate=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $outgoingrate=$row['outgoing'];
    mysqli_free_result($result);
    mysqli_close($link);
    $outgoingrate = number_format($outgoingrate, 2, '.', ',');
    return $outgoingrate;
}

function getcurrentkm()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="SELECT max(fuel_cost_distance) as currentmile FROM fuelconsumtiondb.fuel_cost_tbl;";

    $currentmile=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $currentmile=$row['currentmile'];
    mysqli_free_result($result);
    mysqli_close($link);
    $currentmile = number_format($currentmile, 0, '.', ',');
    return $currentmile;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Fuel Manager</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Fuel Manager</a>
                <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#ViewFuelRecord">ดู Record การเติม</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#ViewMaintennanceRecord">ดู Record ซ่อมบำรุง</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">บันทึกข้อมูล</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead bg-image text-white text-center" style="background-image: url('/images/bg-space.jpeg'); background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: 100% 100%;">
            <div class="container d-flex align-items-center flex-column">
                <h1 class="masthead-heading text-uppercase mb-5">Sangkaya</h1>
                <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-5 rounded" src="images/civic-fc.png"  alt="Cinque Terre" />
                <!-- Masthead Heading-->
                <h3 class="text-uppercase mb-0">Civic FD 2006</h3>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0">ระยะทางสะสม : <?php echo getcurrentkm(); ?> กิโลเมตร</p>
                <p class="masthead-subheading font-weight-light mb-0">ซ่อมบำรุงไปทั้งหมด : <?php echo getmaintenancecost(); ?> บาท</p>
                <p class="masthead-subheading font-weight-light mb-0">เติมน้ำมันไปทั้งหมด : <?php echo getfuelcost(); ?> บาท</p>
                <p class="masthead-subheading font-weight-light mb-0">เติมแก๊สไปทั้งหมด : <?php echo getgascost(); ?> บาท</p>
                <br>
                <p class="masthead-subheading font-weight-light mb-0"> ข้อมูลตั้งแต่เริ่มต้น </p>
                <p class="masthead-subheading font-weight-light mb-0">โดยรวมต่อกิโล : <?php echo getoveralloutgoingrate(); ?> Bath/KM. </p>
                <p class="masthead-subheading font-weight-light mb-0">ค่าเชื้อเพลิงต่อกิโล : <?php echo getfueloutgoingrate(); ?> Bath/KM.</p>
                <p class="masthead-subheading font-weight-light mb-0">ค่า gas ต่อกิโล : <?php echo getgasoutgoingrate(); ?> Bath/KM.</p>
                <br>
                <p class="masthead-subheading font-weight-light mb-0"> ข้อมูลตั้งแต่สามเดือนย้อนหลัง </p>
                <p class="masthead-subheading font-weight-light mb-0">ค่าเชื้อเพลิงต่อกิโล : <?php echo getfueloutgoingrate3month(); ?> Bath/KM.</p>
                <p class="masthead-subheading font-weight-light mb-0">ค่า gas ต่อกิโล : <?php echo getgasoutgoingrate3month(); ?> Bath/KM.</p>
            </div>
        </header>

        <section class="page-section portfolio" id="ViewFuelRecord">
            <div class="container">
                <!-- ViewFuelRecord Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Record การเติม</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- ViewFuelRecord Grid Items-->
                <div class="row justify-content-center">
                  
                <?php
    
                    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
                    if (mysqli_connect_errno()) {
                        printf("Connect failed: %s\n", mysqli_connect_error());
                        exit();
                    }
                    if (!mysqli_set_charset($link, "utf8")) {
                        printf("Error loading character set utf8: %s\n", mysqli_error($link));
                    } else {
                        //printf("Current character set: %s\n", mysqli_character_set_name($link));
                    }

                    $sql="SELECT * FROM fuelconsumtiondb.fuel_cost_tbl where DATE(fuel_cost_date) >= (DATE(NOW()) - INTERVAL 90 DAY) and (fuel_cost_type = 'เติมแก๊ส' or  fuel_cost_type = 'เติมน้ำมัน');";

                    $result = mysqli_query($link,$sql);
                    
                    if(mysqli_affected_rows($link))
                    {
                        $i=0;
                        while ($row = mysqli_fetch_assoc($result))
                        {
                            if($row['fuel_cost_type']!='เริ่มต้น')
                            {
                                if ($i%2==1)
                                {
                                    $mytextcolor="text-white";
                                    $mycardbg="bg-secondary";
                                }
                                else
                                {
                                    $mytextcolor="text-white";
                                    $mycardbg="bg-primary";
                                }
                                if ($row['fuel_cost_type']=='ซ่อมบำรุง'){
                                    $detailshow='<p class="card-text">โดย'.$row['fuel_cost_detail'].'.</p>';
                                }
                                else{
                                    $detailshow='<p class="card-text">ในราคา'.$row['fuel_cost_detail'].' บาท.</p>';
                                }
                                
                                echo '  <div class="col-md-6 col-lg-4 mb-5">
                                        <div class="card '.$mycardbg.'">
                                            <img class="card-img-top" src="images/civic-fc.png" alt="Card image cap">
                                            <div class="card-body text-center '.$mytextcolor.'">
                                                <h5 class="card-title">ได้ทำการ'.$row['fuel_cost_type'].'</h5>
                                                '.$detailshow.'
                                                <p class="card-text">ที่ระยะ '.$row['fuel_cost_distance'].' km.</p>
                                                <p class="card-text">ค่าใช้จ่าย '.$row['fuel_cost_baht'].' บาท.</p>
                                                <p class="card-text">เมื่อ '.$row['fuel_cost_date'].'</p>
                                            </div>
                                        </div>
                                    </div>';
                                    $i++;
                            }
                            
                        }
                    }
                    else
                    {
                        echo '  <div class="card bg-primary text-white mb-1">
                                                    <div class="card-header text-white text-center"><h3>ข้อมูลว่างเปล่า</h3></div>
                                                        <div class="card-body">
                                                            <h3 class="card-text text-center"> DataBase is Empty.</h3>
                                                        </div>
                                                    <div class="card-footer text-center"><h3>กรุณากรอกข้อมูล</h3></div>
                                                </div>';
                    }
                    mysqli_free_result($result);
                    mysqli_close($link);
                ?>
                </div>
            </div>
        </section>

        <section class="page-section portfolio bg-secondary" id="ViewMaintennanceRecord">
            <div class="container">
                <!-- ViewMaintennanceRecord Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-primary mb-0">Record การบำรุงรักษา</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- ViewMaintennanceRecord Grid Items-->
                <div class="row justify-content-center">
                  
                <?php
    
                    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
                    if (mysqli_connect_errno()) {
                        printf("Connect failed: %s\n", mysqli_connect_error());
                        exit();
                    }
                    if (!mysqli_set_charset($link, "utf8")) {
                        printf("Error loading character set utf8: %s\n", mysqli_error($link));
                    } else {
                        //printf("Current character set: %s\n", mysqli_character_set_name($link));
                    }

                    $sql="SELECT * FROM fuelconsumtiondb.fuel_cost_tbl where fuel_cost_type = 'ซ่อมบำรุง';";

                    $result = mysqli_query($link,$sql);
                    
                    if(mysqli_affected_rows($link))
                    {
                        $i=0;
                        while ($row = mysqli_fetch_assoc($result))
                        {
                            if($row['fuel_cost_type']!='เริ่มต้น')
                            {
                                if ($i%2==1)
                                {
                                    $mytextcolor="text-white";
                                    $mycardbg="bg-info";
                                }
                                else
                                {
                                    $mytextcolor="text-white";
                                    $mycardbg="bg-primary";
                                }
                                if ($row['fuel_cost_type']=='ซ่อมบำรุง'){
                                    $detailshow='<p class="card-text">โดย'.$row['fuel_cost_detail'].'.</p>';
                                }
                                else{
                                    $detailshow='<p class="card-text">ในราคา'.$row['fuel_cost_detail'].' บาท.</p>';
                                }
                                
                                echo '  <div class="col-md-6 col-lg-4 mb-5">
                                        <div class="card '.$mycardbg.'">
                                            <img class="card-img-top" src="images/civic-fc.png" alt="Card image cap">
                                            <div class="card-body text-center '.$mytextcolor.'">
                                                <h5 class="card-title">ได้ทำการ'.$row['fuel_cost_type'].'</h5>
                                                '.$detailshow.'
                                                <p class="card-text">ที่ระยะ '.$row['fuel_cost_distance'].' km.</p>
                                                <p class="card-text">ค่าใช้จ่าย '.$row['fuel_cost_baht'].' บาท.</p>
                                                <p class="card-text">เมื่อ '.$row['fuel_cost_date'].'</p>
                                            </div>
                                        </div>
                                    </div>';
                                    $i++;
                            }
                            
                        }
                    }
                    else
                    {
                        echo '  <div class="card bg-primary text-white mb-1">
                                                    <div class="card-header text-white text-center"><h3>ข้อมูลว่างเปล่า</h3></div>
                                                        <div class="card-body">
                                                            <h3 class="card-text text-center"> DataBase is Empty.</h3>
                                                        </div>
                                                    <div class="card-footer text-center"><h3>กรุณากรอกข้อมูล</h3></div>
                                                </div>';
                    }
                    mysqli_free_result($result);
                    mysqli_close($link);
                ?>
                </div>
            </div>
        </section>

        <!-- Contact Section-->
        <section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">บันทึกการเติม</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                        <form action="fuelrecordcheck.php" method="POST">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <select class="form-control" name="cost_type" id="exampleFormControlSelect1" required="required" style="text-align-last: center;">
                                    <option value="">ประเภทค่าใช้จ่าย</option>
                                    <option value="เติมน้ำมัน">เติมน้ำมัน</option>
                                    <option value="เติมแก๊ส">เติมแก๊ส</option>
                                    <option value="ซ่อมบำรุง">ซ่อมบำรุง</option>
                                    </select>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>เลขไมค์ล่าสุด</label>
                                    <input class="form-control" name="cost_mile" type="number" placeholder="เลขไมค์ล่าสุด" required="required" data-validation-required-message="Please enter your name." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>จำนวนเงินที่จ่าย</label>
                                    <input class="form-control" name="cost_price" type="number" step="0.01" placeholder="จำนวนเงินที่จ่าย" required="required" data-validation-required-message="Please enter your email address." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>รายละเอียดค่าใช้จ่าย</label>
                                    <input class="form-control" name="cost_detail" type="text" placeholder="รายละเอียดค่าใช้จ่าย" required="required" data-validation-required-message="Please enter your phone number." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <br />
                            <div class="form-group text-center"><button class="btn btn-primary btn-xl text-center" type="submit">บันทึก</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">
                            2215 John Daniel Drive
                            <br />
                            Clark, MO 65243
                        </p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Freelancer</h4>
                        <p class="lead mb-0">
                            Freelance is a free to use, MIT licensed Bootstrap theme created by
                            <a href="http://startbootstrap.com">Start Bootstrap</a>
                            .
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © Your Website 2020</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
       
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
