<?php

function getprevious_mile()
{
    $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    if (!mysqli_set_charset($link, "utf8")) {
        printf("Error loading character set utf8: %s\n", mysqli_error($link));
    } else {
        //printf("Current character set: %s\n", mysqli_character_set_name($link));
    }

    $sql="SELECT max(fuel_cost_distance) as previous_mile FROM fuelconsumtiondb.fuel_cost_tbl;";
    $previous_mile=0;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    $previous_mile=$row['previous_mile'];
    mysqli_free_result($result);
    mysqli_close($link);
    return $previous_mile;

    mysqli_query($link,$sql);
    mysqli_close($link);    
}

    $current_mile=$_POST['cost_mile'];
    $previous_mile_fromdb=getprevious_mile();
    $trip_distance=$_POST['cost_mile']-$previous_mile_fromdb;
    $cost_price=$_POST['cost_price'];
    $cost_detail=$_POST['cost_detail'];
    $cost_type=$_POST['cost_type'];

    $current_mile_toshow = number_format($current_mile, 0, '.', ',');
    $previous_mile_fromdb_toshow = number_format($previous_mile_fromdb, 0, '.', ',');
    $trip_distance_toshow = number_format($trip_distance, 0, '.', ',');
   

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Fuel Manager</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Fuel Manager</a>
            </div>
        </nav>


        <section class="page-section portfolio bg-primary " id="portfolio">
            <div class="container">
                <!-- Portfolio Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">ข้อมูลการเติม</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <h3 class="text-center text-uppercase text-secondary mb-3"><?php echo date("Y-m-d H:i:s"); ?></h3>
                    <!-- Portfolio Item 2-->
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="card" ">
                            <!-- <img class="card-img-top" src="images/civic-fc.png" alt="Card image cap"> -->
                            <div class="card-body text-center">
                                <p class="card-text"><?php echo "ไมล์สะสมเดิม : <b>".$previous_mile_fromdb_toshow."</b> km. ";?></p>
                                <p class="card-text"><?php echo "ไมล์สะสมล่าสุด : <b>".$current_mile_toshow."</b> km. "; ?></p>
                                <p class="card-text"><?php echo "ระยะทางทริป : <b>".$trip_distance_toshow."</b> กิโลเมตร"; ?></p>
                                <p class="card-text"><?php echo "ได้ทำการ <b>: ".$cost_type."</b>"; ?></p>
                                <p class="card-text"><?php echo "ด้วยเงิน : <b>".$cost_price."</b> บาท "; ?></p>
                            </div>
                            <form action="fuelrecordtodb.php" method="POST">
                                <input type="hidden" name="current_mile" value="<?php echo $current_mile; ?>">
                                <input type="hidden" name="cost_type" value=<?php echo $cost_type; ?>>
                                <input type="hidden" name="cost_detail" value=<?php echo $cost_detail; ?>>
                                <input type="hidden" name="cost_price" value=<?php echo $cost_price; ?>>
                                <div class="form-group text-center"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">บันทึก</button></div>
                                <div class="form-group text-center mt-1"><a href="index.php" class="btn btn-danger btn-xl" id="sendMessageButton" type="cancel">ยกเลิก</a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        
     
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © Your Website 2020</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
       
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
