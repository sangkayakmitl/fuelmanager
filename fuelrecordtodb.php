
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Fuel Manager</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Fuel Manager</a>
            </div>
        </nav>

        <section class="page-section portfolio bg-primary " id="portfolio">
            <div class="container">
                <!-- Portfolio Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">บันทึกข้อมูล</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">บันทึกข้อมูลสำเร็จ</h2>
              
            </div>
        </section>

        <?php
            $link = mysqli_connect('localhost', 'sangkapundba', '4^g-ksbo','fuelconsumtiondb');
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            if (!mysqli_set_charset($link, "utf8")) {
                printf("Error loading character set utf8: %s\n", mysqli_error($link));
            } else {
                //printf("Current character set: %s\n", mysqli_character_set_name($link));
            }
        
            //$sql="INSERT INTO `fuel_record_tbl` (`fuel_record_username`, `fuel_record_mile`, `fuel_record_liter`, `fuel_record_cost`, `fuel_record_kmtrip`, `fuel_record_consumption`, `fuel_record_costrate`, `fuel_record_datetime`) 
            //VALUES ('sangkaya', '".$_POST['current_mile']."','".$_POST['fuelnumber']."','".$_POST['fuelcost']."', '".$_POST['trip_distance']."', '".$_POST['fuelconsomtion']."', '".$_POST['fuelcostrate']."', now());";
            $sql="INSERT INTO `fuelconsumtiondb`.`fuel_cost_tbl` (`fuel_cost_username`, `fuel_cost_distance`, `fuel_cost_type`, `fuel_cost_baht`, `fuel_cost_date`, `fuel_cost_detail`) 
            VALUES ('sangkayakmitl', '".$_POST['current_mile']."', '".$_POST['cost_type']."', '".$_POST['cost_price']."', now(), '".$_POST['cost_detail']."');";
            
            mysqli_query($link,$sql);
            mysqli_close($link);  
        ?>
        <meta http-equiv="refresh" content=".5;url=index.php" />

        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © Your Website 2020</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
       
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
